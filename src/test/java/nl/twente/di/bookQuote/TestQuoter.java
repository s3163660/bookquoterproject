package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("1");
        Assertions.assertEquals(10.0, price, 0.0, "Price of Book 1");
        price = quoter.getBookPrice("2");
        Assertions.assertEquals(45.0, price, 0.0, "Price of Book 2");
        price = quoter.getBookPrice("3");
        Assertions.assertEquals(20.0, price, 0.0, "Price of Book 3");
        price = quoter.getBookPrice("4");
        Assertions.assertEquals(35.0, price, 0.0, "Price of Book 4");
        price = quoter.getBookPrice("5");
        Assertions.assertEquals(50.0, price, 0.0, "Price of Book 5");
        price = quoter.getBookPrice("0");
        Assertions.assertEquals(0.0, price, 0.0, "Price of Book other");
    }
}